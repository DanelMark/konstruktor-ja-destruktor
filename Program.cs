﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3nädal2päev
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene("Henn", 64);

        }
    }
    class Inimene
    {
        public string Nimi { get; set; }

        public int Vanus { get; set; }

        
        static Inimene()    // konstructor, classi nimega. on selline asi mis käivitatakse esimest korda selle klassi poole pöördumisel. Se on Static konstructor
        {
            Console.WriteLine("Algab Inimkond");
        }    

        public Inimene() : this("tundmatu", 0) { }
        public Inimene(string nimi, int vanus)
        {
            //(Nimi, Vanus) = (nimi, vanus);  - see siin laisa inimese valem, ilus ja õige oleks teha nii:
            this.Nimi = Nimi;
            this.Vanus = vanus; // this on muutuja mis on seda tüüpi ja mudelmeetotitel on this see mis oli punkti ees.meetodile "trüki välja" panen ette this.

            Console.WriteLine($"tehti{this}");
        }

        ~Inimene()        // see siin on Desdructor  Selle ~ märgi saav SHIFT + üleval vasakul esimene nupp mis ei tee midagi ja kui vajutan SPACE siis tulebki ~ 
                          // Destructorit läheb vaja kui mahukad , väärtuslikud asjad tuleks midagi teha ehk siis märgib need ära, et äkki on väärtuslik
        {
            Console.WriteLine($"{this} eemaldatakse mälust");
        }
        public override string ToString()
        {
            return $"{Nimi} {Vanus}";
        }
    }
}
